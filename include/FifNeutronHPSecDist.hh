///////////////////////////////////////////////////////////////////////////////
//                   Spectrum of radiative neutron capture by Gadolinium           
//                                    version 1.0.0                                
//                                    (Sep.09.2005)                               

//                Author : karim.zbiri@subatech.in2p3.fr                  

//This file contains the gammas spectrum produced in radiative capture of 
//neutrons by gadolinium.
//This work is adapted from earlier work in geant3 for chooz 1.

//First version by Karim Zbiri, April, 2005
///////////////////////////////////////////////////////////////////////////////

#ifndef FifCaptureGammas_hh
#define FifCaptureGammas_hh

#include "FifDataLoader.hh"
#include "FifNuclideID.hh"
#include <G4ReactionProductVector.hh>
#include <G4Fragment.hh>
#include <G4ThreeVector.hh>
#include <map>
#include <string>

namespace Fifrelin{

class NeutronHPSecDist {
public:
	/**
	 * @brief A distribution of secondary particles associated with a neutron capture on a nucleus (Z,A)
	 *
	 * The distribution is read from an associated Fifrelin data file `<Z><A+1>_3.txt` on path `GEANT4_FIFRELIN_DATA`.
	 * @param z The atomic number (=number of protons) of the nucleus
	 * @param a The atomic weight (=number of nuleons) of the nucleus _before_ the neutron capture
	 * @throw If the associated file can not be found or read an exception is thrown.
	 */
	NeutronHPSecDist(G4int z, G4int a) noexcept(false);
	~NeutronHPSecDist();

	/**
	 * @brief Get the secondary cascade for a neutron capture on a nucleus
	 *
	 * @param nucleus The nucleus on which the neutron was captured
	 * @return The secondary products
	 */
	G4ReactionProductVector* GetSecondaries(G4double neutronKE, G4Fragment* nucleus_cm, G4int nucleusA=0, G4int nucleusZ=0);
	

private:
	/**
	 * @brief Get a random direction
	 * @return The direction
	 */
	G4ThreeVector GetRandomDirection() const;

	/**
	 * @brief The atomic weight of the associated nucleus
	 *
	 * The default value is `0`.
	 */
	G4int A {0};

	/**
	 * @brief The atomic number of the associated nucleus
	 *
	 * The default value is `0`.
	 */
	G4int Z {0};

	/**
	 * @brief The associated Fifrelin data file
	 */
	DataLoader* File {nullptr};

	/**
	 * @brief The path to the Fifrelin data files
	 *
	 * It is set via the environmental variable `GEANT4_FIFRELIN_DATA`.
	 */
	G4String Path {""};
	
	G4String fileName {""};
};

}

#endif
