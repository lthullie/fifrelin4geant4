
///////////////////////////////////////////////////////////////////////////////
//                   Spectrum of radiative neutron capture by Gadolinium           
//                                    version 1.0.0                                
//                                    (Sep.09.2005)                               

//                Author : karim.zbiri@subatech.in2p3.fr                  

//Modified class from original G4NeutronHPCaptureFS class to deexcite and
//add correctly the secondary to the hadronic final state

// Karim Zbiri, Aug, 2005
///////////////////////////////////////////////////////////////////////////////

#include "FifNeutronHPCaptureFS.hh"

#include "FifNuclideID.hh"
//#include <bits/std_abs.h>
#include <CLHEP/Units/SystemOfUnits.h>
#include <CLHEP/Vector/LorentzVector.h>
#include <CLHEP/Vector/ThreeVector.h>
#include <G4DynamicParticle.hh>
#include <G4Fragment.hh>
#include <G4HadFinalState.hh>
#include <G4IonTable.hh>
#include <G4LorentzVector.hh>
#include <G4Material.hh>
#include <G4Neutron.hh>
#include <G4NeutronHPDataUsed.hh>
#include <G4NeutronHPNames.hh>
#include <G4NeutronHPPhotonDist.hh>
#include <G4NucleiProperties.hh>
#include <G4Nucleus.hh>
#include <G4ParticleDefinition.hh>
#include <G4ParticleHPDataUsed.hh>
#include <G4ParticleHPNames.hh>
#include <G4ParticleHPPhotonDist.hh>
#include <G4ParticleTable.hh>
#include <G4Proton.hh>
#include <G4ReactionProduct.hh>
#include <G4ReactionProductVector.hh>
#include <G4ThreeVector.hh>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <vector>

using namespace Fifrelin;

NeutronHPCaptureFS::NeutronHPCaptureFS(){
    hasXsec = false;
}

NeutronHPCaptureFS::~NeutronHPCaptureFS(){
	delete nucleus; nucleus = nullptr;
	delete theFSSecondaries; theFSSecondaries = nullptr;
}

G4HadFinalState* NeutronHPCaptureFS::ApplyYourself(
		const G4HadProjectile &theTrack) {
	//std::cout << "NeutronHPCaptureFS::ApplyYourself()" << std::endl;
	G4int i;
	G4DynamicParticle *theTwo;
	G4ReactionProduct theTarget;
	G4Nucleus aNucleus;
	G4ReactionProduct theNeutron;

// js real 27/3/2015. from geant 4.10 theResult is not a G4HadFinalState anymore but a G4Cache< G4HadFinalState* > theResult;
// this is equivalent to a std::vector in this context, But I don't know if this vector is filled nor its size
// for now we use only the last element (if it exist) and create a new one if none exist
	if (theResult.Get() == NULL)
		theResult.Put(new G4HadFinalState);
	theResult.Get()->Clear();

	// prepare neutron
	G4double eKinetic = theTrack.GetKineticEnergy();
	const G4HadProjectile *incidentParticle = &theTrack;
	theNeutron = const_cast<G4ParticleDefinition*>(incidentParticle->GetDefinition());
	theNeutron.SetMomentum(incidentParticle->Get4Momentum().vect());
	theNeutron.SetKineticEnergy(eKinetic);

	// prepare target
	G4double eps = 0.0001;
	if (targetMass < 500 * CLHEP::MeV)
		targetMass = (G4NucleiProperties::GetNuclearMass(
				static_cast<G4int>(theBaseA + eps),
				static_cast<G4int>(theBaseZ + eps)))
				/ G4Neutron::Neutron()->GetPDGMass();

	G4ThreeVector neutronVelocity = 1. / G4Neutron::Neutron()->GetPDGMass() * theNeutron.GetMomentum();
	G4double temperature = theTrack.GetMaterial()->GetTemperature();
	theTarget = aNucleus.GetBiasedThermalNucleus(targetMass, neutronVelocity, temperature);

	// go to nucleus rest system
	theNeutron.Lorentz(theNeutron, -1 * theTarget);
	eKinetic = theNeutron.GetKineticEnergy();

	// ******************************************************
	// Extract FIFRELIN data
	// ******************************************************
	G4ReactionProductVector *theProducts = NULL;
	
	// ******************************************************
	// Define the compound nucleus from (Z,A) to (Z, A+1)
	// ******************************************************
	G4ThreeVector aCMSMomentum = theNeutron.GetMomentum() + theTarget.GetMomentum();
	G4LorentzVector p4(aCMSMomentum, theTarget.GetTotalEnergy() + theNeutron.GetTotalEnergy());
	nucleus = new G4Fragment(static_cast<G4int>(theBaseA + 1), static_cast<G4int>(theBaseZ), p4);
	
	/// Add the neutron energy to the neutron capture deexcitation computed 
	/// from Sn for En=25 meV so the initial state is E_initial = Sn+En
	/// the assumption is made that the neutron orbital angular momentum is still l=0 and not higher
	G4double neutronKE = theNeutron.GetKineticEnergy();
	
	theProducts = theFSSecondaries->GetSecondaries(neutronKE, nucleus, theBaseA, theBaseZ); ///gammas and electrons in the lab frame

	

	G4int nProducts = 0;
	if (theProducts != NULL)
		nProducts = theProducts->size();

	/*for (i = 0; i < nProducts; i++) {
		G4Fragment *theOne;
		G4ThreeVector pGamma(theProducts->operator[](i)->GetMomentum());
		G4LorentzVector gamma(pGamma, theProducts->operator[](i)->GetTotalEnergy());
		theOne = new G4Fragment(gamma, theProducts->operator[](i)->GetDefinition());
		UpdateNucleus(theOne); /// update the G4Fragment "nucleus"
	}*/

	///Commented out on 2023/01/16
	
	/*theTwo = new G4DynamicParticle;
	G4ParticleDefinition* def = G4ParticleTable::GetParticleTable()->GetIonTable()->GetIon(static_cast<G4int>(theBaseZ), static_cast<G4int>(theBaseA + 1), 0);
	if (!def) {
		printf( "FifNeutronHPCaptureFS::ApplyYourself  can't find ion for theBaseZ=%f   theBaseA=%f\n", theBaseZ, theBaseA);
	}
	theTwo->SetDefinition(def);*/
	
	
	///Compute lab. frame energy and momentum of the recoil nucleus
	
	///Commented out on 2023/01/16
	/*G4ThreeVector recoilNucleusMomentum(nucleus->GetMomentum()); ///momentum given by the incoming neutron
	G4ThreeVector averageMomentum(0,0,0);
	for (i = 0; i < nProducts; i++) {
		averageMomentum += theProducts->operator[](i)->GetMomentum();
	}*/
	
	/*theTwo->SetMomentum(recoilNucleusMomentum+averageMomentum);
	printf( "FifNeutronHPCaptureFS::ApplyYourself ion for theBaseZ=%f   theBaseA=%f\n", theBaseZ, theBaseA);
	G4double ke_1 = theTwo->GetKineticEnergy();
	G4cout<<"v1/ theTwo->GetKineticEnergy()="<<ke_1<<G4endl;*/
	
	///Commented out on 2023/01/16
	//theTwo->SetMomentum(averageMomentum);
	
	
	/*G4double ke_2 = theTwo->GetKineticEnergy();
	G4cout<<"v2/ theTwo->GetKineticEnergy()="<<ke_2<<" MeV"<<G4endl;*/
	//G4cout<<"difference="<<(ke_1-ke_2)/ke_2*100<<" %"<<G4endl;
	//getchar();
	/// Commented because Fifrelin energies are already given the laboratory frame
	// back to lab system
	/*for (i = 0; i < nProducts; i++)
		theProducts->operator[](i)->Lorentz(*(theProducts->operator[](i)), theTarget);*/

	/// Commented because one wants to generalize this
	// Recoil, if only one gamma
	/*if (1 == nProducts) {
		G4DynamicParticle *theOne = new G4DynamicParticle;
		G4ParticleDefinition *aRecoil = G4IonTable::GetIonTable()->FindIon(static_cast<G4int>(theBaseZ), static_cast<G4int>(theBaseA + 1));

		if (!aRecoil) {
			printf("FifNeutronHPCaptureFS::ApplyYourself ##aRecoil## can't find ion for theBaseZ=%f   theBaseA=%f\n", theBaseZ, theBaseA + 1);
		}
		theOne->SetDefinition(aRecoil);
		// Now energy;
		// Can be done slightly better @
		G4ThreeVector aMomentum 	= theTrack.Get4Momentum().vect() + theTarget.GetMomentum() - theProducts->operator[](0)->GetMomentum();
		G4ThreeVector theMomUnit 	= aMomentum.unit();
		G4double aKinEnergy 		= theTrack.GetKineticEnergy() + theTarget.GetKineticEnergy(); // gammas come from Q-value
		G4double theResMass 		= aRecoil->GetPDGMass();
		G4double theResE 			= aRecoil->GetPDGMass() + aKinEnergy;
		G4double theAbsMom 			= std::sqrt(theResE * theResE - theResMass * theResMass);
		G4ThreeVector theMomentum 	= theAbsMom * theMomUnit;
		theOne->SetMomentum(theMomentum);

		theResult.Get()->AddSecondary(theOne);
	}*/

	//****************************************************************************
	// Now fill in the gammas and electrons with their lab caracteristics
	//****************************************************************************
	for (i = 0; i < nProducts; i++) {
		// back to lab system
		G4DynamicParticle *theOne = new G4DynamicParticle;
		const G4ParticleDefinition *mphot = theProducts->operator[](i)->GetDefinition();
		if (!mphot) {
			printf(
					"FifNeutronHPCaptureFS::ApplyYourself can't find definition for theProducts\n");
		}
		theOne->SetDefinition(mphot);
		theOne->SetMomentum(theProducts->operator[](i)->GetMomentum());
		theResult.Get()->AddSecondary(theOne);

		delete theProducts->operator[](i);
	}
	delete theProducts;

	//ADD deexcited nucleus
	///Commented ou on 2023/01/16
	//theResult.Get()->AddSecondary(theTwo);

	// clean up the primary neutron
	theResult.Get()->SetStatusChange(stopAndKill);
	return theResult.Get();
}

void NeutronHPCaptureFS::UpdateNucleus(const G4Fragment *product) {

	G4LorentzVector p4Gamma = product->GetMomentum();
	G4ThreeVector pGamma(p4Gamma.vect());

	G4LorentzVector p4Nucleus(nucleus->GetMomentum());

	G4double ma1 =
			G4ParticleTable::GetParticleTable()->GetIonTable()->GetIonMass(
					static_cast<G4int>(nucleus->GetZ()),
					static_cast<G4int>(nucleus->GetA()));
	G4double ma2 = nucleus->GetZ() * G4Proton::Proton()->GetPDGMass()
			+ (nucleus->GetA() - nucleus->GetZ())
					* G4Neutron::Neutron()->GetPDGMass();

	G4double Mass = std::min(ma1, ma2);

	G4double newExcitation = p4Nucleus.mag() - Mass
			- product->GetMomentum().e();

	if (newExcitation < 0)
		newExcitation = 0;

	G4ThreeVector p3Residual(p4Nucleus.vect() - pGamma);
	G4double newEnergy = std::sqrt(
			p3Residual * p3Residual
					+ (Mass + newExcitation) * (Mass + newExcitation));
	G4LorentzVector p4Residual(p3Residual, newEnergy);

	// Update excited nucleus parameters
	nucleus->SetMomentum(p4Residual);
	return;
}

#if G4VERSION_NUMBER <905 //modif AS for geant4.9.6
 void NeutronHPCaptureFS::Init (G4double A, G4double Z, G4String & /*dirName*/, G4String & )
#elif G4VERSION_NUMBER<1020
 void NeutronHPCaptureFS::Init (G4double A, G4double Z, G4int /*M*/, G4String & /*dirName*/, G4String & /*aFSType*/)
#else
 void NeutronHPCaptureFS::Init (G4double A, G4double Z, G4int /*M*/, G4String & /*dirName*/, G4String & /*aFSType*/, G4ParticleDefinition* )
#endif
{
	//If already set ...
	if(theFSSecondaries){
		//... then delete it ...
		delete theFSSecondaries;
	}
	//... and set it anew
	std::cout << "FifNeutronHPCaptureFS::Init - (Z,A+1)=(" << Z <<"," << A+1 << ")" << std::endl;
	theFSSecondaries = new NeutronHPSecDist(Z, A+1);
}

G4NeutronHPFinalState* Fifrelin::NeutronHPCaptureFS::New() {
	return new NeutronHPCaptureFS();
}
