///////////////////////////////////////////////////////////////////////////////
//                 Spectrum of radiative neutron capture by Gadolinium            
//                                    version 1.0.0                                
//                                    (Sep.09.2005)                               

//                Author : karim.zbiri@subatech.in2p3.fr                  

//This file contains the gammas spectrum produced in radiative capture of 
//neutrons by gadolinium.
//This work is adapted from earlier work in geant3 for chooz 1.

//First version by Karim Zbiri, April, 2005
///////////////////////////////////////////////////////////////////////////////

#include "FifNeutronHPSecDist.hh"
#include "Files.hh"
#include <CLHEP/Vector/ThreeVector.h>
#include <G4Electron.hh>
#include <G4Gamma.hh>
#include <G4ParticleDefinition.hh>
#include <G4ReactionProduct.hh>
#include <G4ParticleTable.hh>
#include <G4IonTable.hh>
#include <G4Types.hh>
#include <Randomize.hh>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <regex>
#include <stdexcept>
#include <utility>
#include <vector>

using namespace Fifrelin;

NeutronHPSecDist::NeutronHPSecDist(G4int z, G4int a) noexcept(false) : A(a), Z(z) {
	if (!getenv("GEANT4_FIFRELIN_DATA")) {
		throw std::invalid_argument(
				"FifCaptureGamma::FifCaptureGamma - Environment variable GEANT4_FIFRELIN_DATA is not defined!");
	}
	//Terminate path with forward slash if necessary
	Path = Files::TerminatePath(getenv("GEANT4_FIFRELIN_DATA"));
//	std::cout << "FifNeutronHPSecDist::NeutronHPSecDist() - GEANT4_FIFRELIN_DATA is set to " << fileName << std::endl;

	//std::string suffix = "_3"; //"_3"; 

	std::stringstream sstr;
	if(A>10){
		if(A>100){
			sstr << Z << A << ".txt"; //suffix <<
		} else {
			sstr << Z << "0" << A << ".txt"; //suffix <<
		}
	} else {
		sstr << Z << "00" << A << ".txt"; //<< suffix
	}
	
	fileName = Path + sstr.str();
	std::cout << "FifNeutronHPSecDist::NeutronHPSecDist() - Data file is set to " << fileName << std::endl;
	//May throw if file can not be read
	File = new DataLoader(fileName);
}

NeutronHPSecDist::~NeutronHPSecDist() {
	delete File; File = nullptr;
}
//----------------------------------------------------------------------------------------------
G4ReactionProductVector* NeutronHPSecDist::GetSecondaries(G4double neutronKE, G4Fragment* nucleus_cm, G4int nucleusA, G4int nucleusZ) {
	G4ReactionProductVector *theDeesxcitationProducts =
			new G4ReactionProductVector;

	Cascade kinematics = File->GetNextCascade();
	//G4cout<<"----- In FifNeutronHPSecDist::GetSecondaries A= "<<A<<" - Z= "<<Z<<"  fileName="<< fileName<< " -----"<<G4endl;
	// Gammas
	int idx=0;
	if(kinematics[CascadeParticle::Gamma].size()==0){
	std::cout<<"Size : "<<kinematics[CascadeParticle::Gamma].size()<<" "<<"A"<<A<<std::endl;}
	
	for(int j=0; j+2<kinematics[CascadeParticle::Gamma].size();j=j+3){
		double energy=kinematics[CascadeParticle::Gamma][j];
		//std::cout<<energy<<std::endl;
		G4double phi=kinematics[CascadeParticle::Gamma][j+1];
		G4double theta=kinematics[CascadeParticle::Gamma][j+2];
		//std::cout<<theta<<" "<<phi<<std::endl;
		theta = theta*CLHEP::twopi/360.; //conversion from degree to radian
		phi = phi*CLHEP::twopi/360.; //idem
		G4ThreeVector directionFifradina = G4ThreeVector(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
		G4ReactionProduct *theOne = new G4ReactionProduct;
		theOne->SetDefinition(G4Gamma::Gamma());

		if(idx==0){
			energy += neutronKE;
		}
		//G4cout<<"gammas - energy="<<energy<<G4endl;
		theOne->SetTotalEnergy(energy);
		theOne->SetMomentum(energy * directionFifradina);
		theDeesxcitationProducts->push_back(theOne);
		idx++;
	}

	// Electrons
	for (auto energy : kinematics[CascadeParticle::Electron]) {
		G4ReactionProduct *theOne = new G4ReactionProduct;
		theOne->SetDefinition(G4Electron::Electron());
		
		if(idx==0){
			energy += neutronKE;
		}
		//G4cout<<"electron - energy="<<energy<<G4endl;
		theOne->SetKineticEnergy(energy);
		double p = sqrt( theOne->GetKineticEnergy()	* (theOne->GetKineticEnergy() + 2 * G4Electron::Electron()->GetPDGMass()));
		theOne->SetMomentum(p * GetRandomDirection());
		theDeesxcitationProducts->push_back(theOne);
		
		idx++;
	}
	// Nucleus
	if(nucleusA>0 && nucleusZ>0){
		///Define recoiling nucleus
		G4ReactionProduct *theTwo = new G4ReactionProduct;
		G4ParticleDefinition* def = G4ParticleTable::GetParticleTable()->GetIonTable()->GetIon(static_cast<G4int>(nucleusZ), static_cast<G4int>(nucleusA + 1), 0);
		
		if (!def) {
			printf( "FifNeutronHPCaptureFS::ApplyYourself  can't find ion for nucleusZ=%i   nucleusA=%i\n", nucleusZ, nucleusA);
		}
		theTwo->SetDefinition(def);
		
		if(kinematics[CascadeParticle::Nucleus].size()>0){
			//std::cout<<"Compute recoil nucleus energy from IRADINA"<<std::endl;
			for (auto energy : kinematics[CascadeParticle::Nucleus]) {
				theTwo->SetKineticEnergy(energy);
				double p = sqrt( theTwo->GetKineticEnergy()*(theTwo->GetKineticEnergy() +  2 * def->GetPDGMass()));
				
				/// the assumption is made that with FIFRELIN+Iradina that the nucleus recoild direction does not depend on the emitted gamma and electron emission, 
				/// this is not true but can be updated if asked for another FIFRADINA file
				/// the direction information is lost during the FIFRADINA formatting step but the momentum is conserved when computing it
				G4ThreeVector direction = p * GetRandomDirection();
				theTwo->SetMomentum(direction);
			}
		}
		else {	
			//std::cout<<"Compute recoil nucleus energy from momenta"<<std::endl;	
			G4int nProducts = 0;
			if (theDeesxcitationProducts != NULL)	nProducts = theDeesxcitationProducts->size();
			
			G4ThreeVector averageMomentum(0,0,0);
			for (G4int i = 0; i < nProducts; i++) {
				averageMomentum += theDeesxcitationProducts->operator[](i)->GetMomentum();
			}
						
			theTwo->SetMomentum(averageMomentum /*+ nucleus_cm->GetMomentum()*/);	
		}
		///Add the recoil nucleus in the stack
		theDeesxcitationProducts->push_back(theTwo);
	}
	
	return theDeesxcitationProducts;
}

G4ThreeVector Fifrelin::NeutronHPSecDist::GetRandomDirection() const {
	G4double costheta = 2. * G4UniformRand() - 1;
	G4double theta = acos(costheta);
	G4double phi = CLHEP::twopi * G4UniformRand();
	G4double sintheta = sin(theta);

	return G4ThreeVector(
			sintheta * cos(phi),
			sintheta * sin(phi),
			costheta
	);
}
