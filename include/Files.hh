/*
 * Files.hh
 *
 *  Created on: 27 Jan 2022
 *      Author: hkluck
 */

#ifndef SIMULATION_FIFRELIN_INCLUDE_FILES_HH_
#define SIMULATION_FIFRELIN_INCLUDE_FILES_HH_

#include <string>
#include <vector>
#include <regex>    //Need for regular expression

/**
 * Contains helper function to access files
 */
namespace Files{

/**
 * A file path
 */
typedef std::string Path;

/**
 * A collection of paths
 */
typedef std::vector<Files::Path> ColOfPaths;

/**
 * @brief Return the content of a directory that match the given regexp
 *
 * @warning Works only for Linux systems!
 * @warning directoryPath need to be terminated with a forward slash '/'!
 * @todo As soon as Choozerent change to C++17 use std::filesystem.
 * @param directoryPath The path to the directory
 * @param regex Regular expression to filter content
 * @param onlyFiles If true (default value), returns only directory content of type file
 * @throw Throw an exception if the directory cannot be open
 * @return The files that matched the regexp
 */
ColOfPaths GetDirectoryContent(const Files::Path& directoryPath, const std::regex& regex, bool onlyFiles = true) noexcept(false);

/**
 * @brief Add a forward slash '/' at the end of the path if it is missing
 *
 * If the original path terminated already in a forward slash, nothing is done.
 *
 * @param path The original path
 * @return The same path with a trailing '/'
 */
Path TerminatePath(const Path& path);

/**
 * @brief Count the number of lines in a file
 *
 * Following the solution on [stackoverflow: Fastest way to find the number of lines in a text (C++)](https://stackoverflow.com/questions/843154/fastest-way-to-find-the-number-of-lines-in-a-text-c),
 * a file stream is piecewise read into a buffer and then the occurrences of '\n' is counted.
 *
 * @warning Works only for file formats where a new line is indicated by '\n'!
 * @todo Check if unsigned return value is wise, see C++ Core Guideline ES.106
 * @param inputStream An input file stream
 * @param bufferSize The size of the buffer in byte, the default size is 1 MB
 * @return The number of lines
 */
unsigned int GetNbOfLines(std::ifstream &inputStream, int bufferSize = 1048576);
}

#endif /* SIMULATION_FIFRELIN_INCLUDE_FILES_HH_ */
