#ifndef SIMULATION_FIFRELIN_INCLUDE_FIFDATALOADER_HH_
#define SIMULATION_FIFRELIN_INCLUDE_FIFDATALOADER_HH_

#include <G4String.hh>
#include <deque>
#include <fstream>
#include <map>
#include <vector>

class TRandom3;

namespace Fifrelin{

enum CascadeParticle {
	Gamma,
	Electron,
	Nucleus
};

/**
 * A cascade of secondary particles
 *
 * It is a map of #FifProduct and a vector of doubles.
 */
typedef std::map<CascadeParticle, std::vector<double>> Cascade;

class DataLoader {

public:
	DataLoader(G4String file_path, unsigned int bunch_size = 10000000);
	~DataLoader();

	Cascade GetNextCascade();

	/**
	 * @brief Get the path of the associated Fifrelin file.
	 * @return The file path
	 */
	G4String GetFilePath() const;

private:
	void LoadCascades(unsigned int N);

	G4String FilePath;
	std::ifstream InputFile;

	std::deque<Cascade> SelectedGammaCascades;

	unsigned int BunchSize;
	//unsigned int LinesNumber;

	//unsigned int loaded_cascades;

	/**
	 * @brief Points to ROOT's [Mersenne Twister](https://root.cern.ch/doc/master/classTRandom3.html) random number generator
	 */
	TRandom3* RNG;
};

}

#endif /* SIMULATION_FIFRELIN_INCLUDE_FIFDATALOADER_HH_ */
