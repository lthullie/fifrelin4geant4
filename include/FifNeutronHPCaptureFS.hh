
///////////////////////////////////////////////////////////////////////////////
//                    Spectrum of radiative neutron capture by Gadolinium            
//                                    version 1.0.0                                
//                                    (Sep.09.2005)                               

//                Author : karim.zbiri@subatech.in2p3.fr                  

//Modified class from original G4NeutronHPCaptureFS class to deexcite and
//add correctly the secondary to the hadronic final state

// Karim Zbiri, Aug, 2005
///////////////////////////////////////////////////////////////////////////////

#ifndef FifNeutronHPCaptureFS_h
#define FifNeutronHPCaptureFS_h 1


#include "FifNeutronHPSecDist.hh"
#include <G4HadProjectile.hh>
#include <G4NeutronHPFinalState.hh>
#include <G4String.hh>
#include <G4Types.hh>
#include <G4Version.hh>

class G4Fragment;

namespace Fifrelin {

class NeutronHPCaptureFS: public G4NeutronHPFinalState {
public:

	NeutronHPCaptureFS();

	~NeutronHPCaptureFS();

	void UpdateNucleus(const G4Fragment*);

	/*void Init(G4double A, G4double Z, G4int M, G4String &dirName,
			G4String &aFSType, G4ParticleDefinition*) override;*/
			
#if G4VERSION_NUMBER<905 //modif AS for geant4.9.6
  void Init (G4double A, G4double Z, G4String & dirName, G4String & aFSType);
#elif G4VERSION_NUMBER<1020
  void Init (G4double A, G4double Z, G4int M, G4String & dirName, G4String & aFSType);
#else
  void Init (G4double A, G4double Z, G4int M, G4String & dirName, G4String & aFSType, G4ParticleDefinition*) override;
#endif

	G4HadFinalState* ApplyYourself(const G4HadProjectile &theTrack) override;

	G4NeutronHPFinalState* New() override;

private:
	G4Fragment *nucleus {nullptr};
	G4double targetMass {0.};
	NeutronHPSecDist *theFSSecondaries {nullptr};

};

}
#endif
