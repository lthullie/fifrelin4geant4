#include "FifDataLoader.hh"

#include "Files.hh"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <ctime>

using namespace Fifrelin;

DataLoader::DataLoader(G4String file_path,
		unsigned int bunch_size) :
		FilePath(file_path), SelectedGammaCascades(std::deque<Cascade>(0)), BunchSize(
				bunch_size)/*, LinesNumber(0), loaded_cascades(0), RNG(new TRandom3())*/ {

	//Check if input file can be open
	InputFile.open(FilePath);
	if (!InputFile) {
		throw std::invalid_argument(
				"In FifrelinGammaCascadeLoader::Configure, file=" + FilePath
						+ " is not open");
	}

	//Set seed of random number generator
	//RNG->SetSeed();

	//Count number of lines inside the file
	//LinesNumber = Files::GetNbOfLines(InputFile);
	InputFile.clear();
	InputFile.seekg(0, std::ios::beg);
	//InputFile.close();
}

DataLoader::~DataLoader() {
	//delete RNG; RNG = nullptr;
}

//----------------------------------------------------------------------------------------------
Cascade DataLoader::GetNextCascade() {
	//std::cout<<"------ DataLoader::GetNextCascade ------"<<std::endl;
	if (SelectedGammaCascades.size() == 0) {
		LoadCascades(BunchSize);
	}

	Cascade Next(SelectedGammaCascades.front());
	SelectedGammaCascades.pop_front();

	return Next;
}

//----------------------------------------------------------------------------------------------
void DataLoader::LoadCascades(unsigned int N) {
	std::cout<<"-------N----------- "<<N<<std::endl;
	//std::cout<<"In DataLoader::LoadCascades"<<std::endl;
	
	/*clock_t time_start, time_stop;
	time_start = clock();*/
	
	unsigned int nLoops = 0;
	std::string line, word;
	while (!InputFile.eof() && nLoops<N) {
		getline(InputFile, line);
		
		Cascade CurrentGammaCascade = {
				{ CascadeParticle::Electron, std::vector<double>(0) },
				{ CascadeParticle::Gamma, std::vector<double>(0)},
				{ CascadeParticle::Nucleus, std::vector<double>(0)  }
		};
		
		std::stringstream sline;
		sline.str(line);
		word = "";
		double E_lab = 0, time=0.;
		int multiplicity = 0;
		double phi=0;
		double theta=0;
		
		while (sline >> word) {
			if(word=="R"){
				sline >> E_lab;
				CurrentGammaCascade[CascadeParticle::Nucleus].push_back(E_lab*1e-6);
			}
			else {
				sline >> multiplicity;

				for (int i = 0; i < multiplicity; ++i) {
					sline >> E_lab >> time;
					
					if (word == "G"){
						sline >> phi >> theta; 
						CurrentGammaCascade[CascadeParticle::Gamma].push_back(E_lab);
						CurrentGammaCascade[CascadeParticle::Gamma].push_back(phi);
						CurrentGammaCascade[CascadeParticle::Gamma].push_back(theta);
					}
					else if (word == "E")	CurrentGammaCascade[CascadeParticle::Electron].push_back(E_lab);
					else throw std::invalid_argument("In DataLoader::LoadCascades, particle not defined: type="+word);
				}
			}
		}

		//loaded_cascades += 1;
		SelectedGammaCascades.push_back(CurrentGammaCascade);
		
		++nLoops;
		
		///Reinitialize the file at the beginning if no more cascade to read in this file
		if(InputFile.eof()){
			InputFile.clear();
			InputFile.seekg(0, std::ios::beg);
			std::cout<<"In FifDataLoader::LoadCascades rewind file="<<FilePath<<std::endl;
		}
	}
	
	/*time_stop = clock();
	std::cout<<"time_start-time_stop="<<double(time_stop-time_start)/CLOCKS_PER_SEC<<std::endl;*/
	
	/*std::vector<unsigned int> Selection(N);

	for (unsigned int i = 0; i < N; ++i)
		Selection[i] = RNG->Integer(LinesNumber);

	InputFile.open(FilePath.c_str());

	if (!InputFile) {
		throw std::invalid_argument(
				"In FifrelinASCIITreeReader::FifrelinASCIITreeReader, file="
						+ FilePath + " is not open");
	}

	unsigned int line_no = 1;

	auto SelectLine = [&Selection](unsigned int no) {
		bool Select = false;
		for (unsigned int chosen_line : Selection) {
			if (no == chosen_line)
				Select = true;
		}
		return Select;
	};

	std::string line, word;
	
	while (!InputFile.eof()) {
		getline(InputFile, line);
		//std::cout<<SelectLine(line_no)<<" - line="<<line<<std::endl;
		if (SelectLine(line_no)) {
			//std::cout<<"Get this line"<<std::endl;
			Cascade CurrentGammaCascade = {
					{ CascadeParticle::Electron, std::vector<double>(0) },
					{ CascadeParticle::Gamma, std::vector<double>(0)    },
					{ CascadeParticle::Nucleus, std::vector<double>(0)  }
			};
			
			std::stringstream sline;
			sline.str(line);
			word = "";
			double E_lab = 0, time=0.;
			int multiplicity = 0;
			
			while (sline >> word) {
				if(word=="R"){
					sline >> E_lab;
					CurrentGammaCascade[CascadeParticle::Nucleus].push_back(E_lab*1E-6);
				}
				else {
					sline >> multiplicity;
					//std::cout<<"word="<<word<<" - multiplicity="<<multiplicity<<std::endl;
					for (int i = 0; i < multiplicity; ++i) {
						sline >> E_lab >> time;
						
						if (word == "G")		CurrentGammaCascade[CascadeParticle::Gamma].push_back(E_lab);
						else if (word == "E")	CurrentGammaCascade[CascadeParticle::Electron].push_back(E_lab);
						else throw std::invalid_argument("In DataLoader::LoadCascades, particle not defined: type="+word);
						
						//std::cout<<"E_lab="<<E_lab<<" - time="<<time<<std::endl;
					}
				}
			}

			loaded_cascades += 1;
			SelectedGammaCascades.push_back(CurrentGammaCascade);
		}

		++line_no;
	}*/
	

	//InputFile.close();
}

G4String DataLoader::GetFilePath() const {
	return FilePath;
}
