#----------------------------------------------#
## Check if cmake has the required version
cmake_minimum_required(VERSION 2.8...3.20.5)

#----------------------------------------------#
## Declaring the project
project(Fifrelin)

#----------------------------------------------#
# set(Geant4_CONFIG_DEBUG 1)
find_package(Geant4 10.1 REQUIRED CONFIG)
find_package(ROOT 5.20 REQUIRED QUIET)

if(${ROOT_FOUND})
   message(STATUS "ROOT was found!")
endif(${ROOT_FOUND})

#----------------------------------------------#
# Setup Geant4 include directories and compile definitions
include_directories(
  ${PROJECT_SOURCE_DIR}/include
  ${common_SOURCE_DIR}/include
)
include_directories(SYSTEM ${Geant4_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

message("ROOT_INCLUDE_DIRS=" ${ROOT_INCLUDE_DIRS})
message("Geant4_INCLUDE_DIRS=" ${Geant4_INCLUDE_DIRS})

#----------------------------------------------#
# Locate sources and headers for this project
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.hh)

message("PROJECT_SOURCE_DIR=" ${PROJECT_SOURCE_DIR})

#----------------------------------------------#
# Create the shared library
add_library(Fifrelin SHARED ${sources} ${headers})
target_link_libraries(Fifrelin ${Geant4_LIBRARIES} ${ROOT_LIBRARIES})
set_target_properties(Fifrelin PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/lib/)
install(TARGETS Fifrelin DESTINATION ${CMAKE_INSTALL_FULL_LIBDIR})

# if the installation fails with the install(...) instruction above try to replace it by the following one:
# install(TARGETS Fifrelin LIBRARY DESTINATION lib ${CMAKE_INSTALL_FULL_LIBDIR} )
