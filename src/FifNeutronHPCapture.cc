#include "FifNeutronHPCapture.hh"

#include <CLHEP/Units/SystemOfUnits.h>
#include <G4Element.hh>
#include <G4HadronicException.hh>
#include <G4HadronicInteraction.hh>
#include <G4ios.hh>
#include <G4Isotope.hh>
#include <G4Material.hh>
#include <G4ParticleHPManager.hh>
#include <G4ParticleHPReactionWhiteBoard.hh>
#include <G4Threading.hh>
#include <Randomize.hh>
#include <templates.hh>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <string>

using namespace Fifrelin;

void NeutronHPCapture::BuildPhysicsTable(const G4ParticleDefinition&) {
	/*HK: It seems G4NeutronHPManager provide here no functionality beside holding pointers to the NeutronHPChannels.
	 *    As FifNeutronHPChannel can not subclass G4NeutronHPChannel, we can not pass FifNeutronHPChannels to
	 *    G4NeutronHPManager. So comment it out.
	 */
//	G4NeutronHPManager *hpmanager = G4NeutronHPManager::GetInstance();
//	theCapture = hpmanager->GetCaptureFinalStates();
	G4cout << "----------- FIFRELIN nuclide list -------- " << G4endl;
	for(int i=0;i<theFifrelinNuclideList.size();i++){
		G4cout << theFifrelinNuclideList[i] << G4endl;
	}
	if (!G4Threading::IsWorkerThread()) {
		if (theCapture == nullptr)
			//Will take pointers to FifNeutronHPChannel, but since it's a subclass of
			//G4ParticleHPChannel it can be stored in a std::vector<G4ParticleHPChannel*>.
			//A std::vector<Fifrelin::ParticleHPChannel*> would not match the type of theCapture.
			theCapture = new std::vector<NeutronHPChannel*>;
		if (numEle == (G4int) G4Element::GetNumberOfElements())
			return;

		if (theCapture->size() == G4Element::GetNumberOfElements()) {
			numEle = G4Element::GetNumberOfElements();
			return;
		}

		if (!getenv("G4NEUTRONHPDATA"))
			throw G4HadronicException(__FILE__, __LINE__,
					"Please setenv G4NEUTRONHPDATA to point to the neutron cross-section files.");
		dirName = getenv("G4NEUTRONHPDATA");
		G4String tString = "/Capture";
		dirName = dirName + tString;
		//This is the only deviation from standard Geant4 G4NeutronHPCapture:
		//use of Fifrelin::NeutronHPChannel instead of G4NeutronHPChannel
		for (G4int i = numEle; i < (G4int) G4Element::GetNumberOfElements(); i++) {
			(*theCapture).push_back(new NeutronHPChannel(theFifrelinNuclideList));
			(*(*theCapture)[i]).Init((*(G4Element::GetElementTable()))[i], dirName);
			(*(*theCapture)[i]).Register(nullptr);
		}
//		hpmanager->RegisterCaptureFinalStates(theCapture);

	}
	numEle = G4Element::GetNumberOfElements();
	//G4cout<<"In NeutronHPCapture::BuildPhysicsTable numEle="<<numEle<<G4endl;
}

NeutronHPCapture::NeutronHPCapture() : G4HadronicInteraction("NeutronHPCapture"), theCapture(NULL), numEle(0) {
	SetMinEnergy(0.0);
	SetMaxEnergy(20. * CLHEP::MeV);
	/*
	 //    G4cout << "Capture : start of construction!!!!!!!!"<<G4endl;
	 if(!getenv("G4NEUTRONHPDATA"))
	 throw G4HadronicException(__FILE__, __LINE__, "Please setenv G4NEUTRONHPDATA to point to the neutron cross-section files.");
	 dirName = getenv("G4NEUTRONHPDATA");
	 G4String tString = "/Capture";
	 dirName = dirName + tString;
	 numEle = G4Element::GetNumberOfElements();
	 //    G4cout << "+++++++++++++++++++++++++++++++++++++++++++++++++"<<G4endl;
	 //    G4cout <<"Disname="<<dirName<<" numEle="<<numEle<<G4endl;
	 //theCapture = new G4ParticleHPChannel[numEle];
	 //    G4cout <<"G4ParticleHPChannel constructed"<<G4endl;
	 NeutronHPCaptureFS * theFS = new NeutronHPCaptureFS;
	 //for (G4int i=0; i<numEle; i++)
	 //{
	 //  //    G4cout << "initializing theCapture "<<i<<" "<< numEle<<G4endl;
	 //  theCapture[i].Init((*(G4Element::GetElementTable()))[i], dirName);
	 //  theCapture[i].Register(theFS);
	 //}
	 for ( G4int i = 0 ; i < numEle ; i++ )
	 {
	 theCapture.push_back( new G4ParticleHPChannel );
	 (*theCapture[i]).Init((*(G4Element::GetElementTable()))[i], dirName);
	 (*theCapture[i]).Register(theFS);
	 }
	 delete theFS;
	 //    G4cout << "-------------------------------------------------"<<G4endl;
	 //    G4cout << "Leaving NeutronHPCapture::NeutronHPCapture"<<G4endl;
	 */
}

NeutronHPCapture::NeutronHPCapture(std::vector<int> _theFifrelinNuclideList) :
	G4HadronicInteraction("NeutronHPCapture"), theCapture(NULL), numEle(0) {
	SetMinEnergy(0.0);
	SetMaxEnergy(20. * CLHEP::MeV);
	theFifrelinNuclideList = _theFifrelinNuclideList;
}

NeutronHPCapture::~NeutronHPCapture() {
	//delete [] theCapture;
    //G4cout << "Leaving NeutronHPCapture::~NeutronHPCapture - begin"<<G4endl;

	for (std::vector<NeutronHPChannel*>::iterator ite = theCapture->begin(); ite != theCapture->end(); ite++) {
		delete *ite;
	}
	theCapture->clear();
	
	//G4cout << "Leaving NeutronHPCapture::~NeutronHPCapture - end"<<G4endl;
}

#include "G4ParticleHPThermalBoost.hh"
G4HadFinalState* NeutronHPCapture::ApplyYourself(const G4HadProjectile &aTrack,
		G4Nucleus &aNucleus) {

	//if ( numEle < (G4int)G4Element::GetNumberOfElements() ) addChannelForNewElement();
	//G4cout<<"FifNeutronHPCapture::ApplyYourself - first select element ------"<<G4endl;
	G4ParticleHPManager::GetInstance()->OpenReactionWhiteBoard();
	if (getenv("NeutronHPCapture"))
		G4cout << " ####### NeutronHPCapture called" << G4endl;
	const G4Material *theMaterial = aTrack.GetMaterial();
	G4int n = theMaterial->GetNumberOfElements();
	G4int index = theMaterial->GetElement(0)->GetIndex();
	if (n != 1) {
		G4double *xSec = new G4double[n];
		G4double sum = 0;
		G4int i;
		const G4double *NumAtomsPerVolume =
				theMaterial->GetVecNbOfAtomsPerVolume();
		G4double rWeight;
		G4ParticleHPThermalBoost aThermalE;
		for (i = 0; i < n; i++) {
			index = theMaterial->GetElement(i)->GetIndex();
			rWeight = NumAtomsPerVolume[i];
			//xSec[i] = theCapture[index].GetXsec(aThermalE.GetThermalEnergy(aTrack,
			xSec[i] = ((*theCapture)[index])->GetXsec(
					aThermalE.GetThermalEnergy(aTrack,
							theMaterial->GetElement(i),
							theMaterial->GetTemperature()));
			xSec[i] *= rWeight;
			sum += xSec[i];
			
			//G4cout<<"element="<<theMaterial->GetElement(i)->GetName()<<" - SigmaSec="<<xSec[i]<<" - rWeight="<<rWeight<<" - xs="<<((*theCapture)[index])->GetXsec(aThermalE.GetThermalEnergy(aTrack, theMaterial->GetElement(i), theMaterial->GetTemperature()))<<G4endl;
		}
		G4double random = G4UniformRand();
		G4double running = 0;
		for (i = 0; i < n; i++) {
			running += xSec[i];
			index = theMaterial->GetElement(i)->GetIndex();
			
			//if(random<=running/sum) break;
			if (sum == 0 || random <= running / sum)
				break;
		}
		if (i == n)
			i = std::max(0, n - 1);
		delete[] xSec;
	}

	//return theCapture[index].ApplyYourself(aTrack);
	//G4HadFinalState* result = theCapture[index].ApplyYourself(aTrack);
	//G4cout<<"FifNeutronHPCapture -- before captureFS/isotope selection -- selected element="<<  (*G4Element::GetElementTable())[index]->GetName() <<G4endl;
	G4HadFinalState *result = ((*theCapture)[index])->ApplyYourself(aTrack);

	//Overwrite target parameters
	aNucleus.SetParameters(
			G4ParticleHPManager::GetInstance()->GetReactionWhiteBoard()->GetTargA(),
			G4ParticleHPManager::GetInstance()->GetReactionWhiteBoard()->GetTargZ());
	const G4Element *target_element = (*G4Element::GetElementTable())[index];
	const G4Isotope *target_isotope = NULL;
	G4int iele = target_element->GetNumberOfIsotopes();
	for (G4int j = 0; j != iele; j++) {
		target_isotope = target_element->GetIsotope(j);
		if (target_isotope->GetN()
				== G4ParticleHPManager::GetInstance()->GetReactionWhiteBoard()->GetTargA())
			break;
	}
	//G4cout << "Target Material of this reaction is " << theMaterial->GetName() << G4endl;
	//G4cout << "Target Element of this reaction is " << target_element->GetName() << G4endl;
	//G4cout << "Target Isotope of this reaction is " << target_isotope->GetName() << G4endl;
	aNucleus.SetIsotope(target_isotope);

	G4ParticleHPManager::GetInstance()->CloseReactionWhiteBoard();
	return result;
}

const std::pair<G4double, G4double> NeutronHPCapture::GetFatalEnergyCheckLevels() const {
	//return std::pair<G4double, G4double>(10*perCent,10*GeV);
	return std::pair<G4double, G4double>(10 * CLHEP::perCent, DBL_MAX);
}

/*
 void NeutronHPCapture::addChannelForNewElement()
 {
 NeutronHPCaptureFS* theFS = new NeutronHPCaptureFS;
 for ( G4int i = numEle ; i < (G4int)G4Element::GetNumberOfElements() ; i++ )
 {
 G4cout << "NeutronHPCapture Prepairing Data for the new element of " << (*(G4Element::GetElementTable()))[i]->GetName() << G4endl;
 theCapture.push_back( new G4ParticleHPChannel );
 (*theCapture[i]).Init((*(G4Element::GetElementTable()))[i], dirName);
 (*theCapture[i]).Register(theFS);
 }
 delete theFS;
 numEle = (G4int)G4Element::GetNumberOfElements();
 }
 */

G4int NeutronHPCapture::GetVerboseLevel() const {
	return G4ParticleHPManager::GetInstance()->GetVerboseLevel();
}
void NeutronHPCapture::SetVerboseLevel(G4int newValue) {
	G4ParticleHPManager::GetInstance()->SetVerboseLevel(newValue);
}

void NeutronHPCapture::ModelDescription(std::ostream &outFile) const {
	outFile
			<< "High Precision model based on Evaluated Nuclear Data Files (ENDF) for radiative capture reaction of neutrons below 20MeV\n";
}
