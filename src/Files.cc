/*
 * Files.cc
 *
 *  Created on: 27 Jan 2022
 *      Author: hkluck
 */

#include "Files.hh"

#include <stdexcept>//Need for error handling
#include <dirent.h> //Need for file operation
#include <fstream>

//Based on https://www.cppstories.com/2019/04/dir-iterate/
Files::ColOfPaths Files::GetDirectoryContent(const Files::Path &directoryPath, const std::regex& regex, bool onlyFiles)
		noexcept (false) {
	Files::ColOfPaths files;
	struct dirent *entry = nullptr;
	DIR *dp = nullptr;

	//Open directoryPath ...
	dp = opendir(directoryPath.c_str());
	if (dp != nullptr) {
		//... and loop over its content
		while ((entry = readdir(dp))) {
			std::string filePath = directoryPath + std::string(entry->d_name);
			//Test if entry matchs the regexp
			bool isMatch = std::regex_search(filePath, regex);
			//Test if entry is regular file
			bool isFile = (entry->d_type == DT_REG);
			//Add only those entries which match the regexp and ...
			//... are regular files when onlyFiles is required
			//... or of arbitrary type when onlyFiles is not requires
			if(isMatch && (!onlyFiles || (onlyFiles && isFile))){
				files.push_back(filePath);
			}
		}
	} else {
		//... directory cannot be open
		throw std::invalid_argument(
				"In FifCaptureGamma::LoopOverFile - can not open '"
						+ directoryPath + "'!");
	}

	closedir(dp);
	return files;
}

Files::Path Files::TerminatePath(const Path &path) {
	//If last character of path is not a forward slash ...
	if(!(path.at(path.length()-1) == '/')){
		//... add it
		return path + "/";
	}else{
		return path;
	}
}

unsigned int Files::GetNbOfLines(std::ifstream &inputStream, int bufferSize) {
	//Number of lines in the input file stream
	unsigned int nbLines = 0;

	//Buffer
	std::vector<char> buffer(bufferSize);

	//Lambda to read a chunk of bufferSize from the input stream into the buffer
	auto FileRead = [&]() {
		inputStream.read(&buffer[0], buffer.size());
		return inputStream.gcount();
	};

	//Loop over the file stream and read into the buffer
	while (int cc = FileRead()) {
		//Loop over the buffer
		for (int i = 0; i < cc; ++i) {
			//And count the lines by counting the new line-characters
			if (buffer[i] == '\n') {
				nbLines++;
			}
		}
	}

	return nbLines;
}
