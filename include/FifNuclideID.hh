/*
 * FifNuclideID.hh
 *
 *  Created on: 27 Jan 2022
 *      Author: hkluck
 */

#ifndef SIMULATION_FIFRELIN_INCLUDE_FIFNUCLIDEID_HH_
#define SIMULATION_FIFRELIN_INCLUDE_FIFNUCLIDEID_HH_

#include <vector>

namespace Fifrelin{
/**
 * A pair of Z and A values to identify a nuclide
 */
struct NuclideID {
	NuclideID() : NuclideID(0, 0) {};
	NuclideID(int z, int a) : Z(z), A(a) {
	};
	inline bool operator==(const NuclideID& rhs) const{
		return ((rhs.A == A) && (rhs.Z == Z));
	}
	inline bool operator<(const NuclideID& rhs) const{
		//No particular reason for this sorting scheme ...
		return ((rhs.A < A) || (rhs.Z < Z));
	}
	int Z;
	int A;
};

typedef std::vector<NuclideID> VecOfNuclideIDs;

}

#endif /* SIMULATION_FIFRELIN_INCLUDE_FIFNUCLIDEID_HH_ */
