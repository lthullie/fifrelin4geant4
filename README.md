# Fifrelin4Geant4

Fifrelin4Geant4 is an interface library that allows to use nuclear de-excitation files created with <a href="#fifrelin-ref">FIFRELIN</a> within a [Geant4](https://geant4.web.cern.ch/) application. It was developed for the <a href="#crab-ref">CRAB experiment</a> based on previous works performed within the STEREO and Double Chooz collaborations.
Fifrelin4Geant4 can easily be used with minor modifications depending on the dataset format with:

	- 155Gd(nth,g) and 157Gd(nth,g) de-excitation cascade datasets for neutrino experiment with Gd-loaded detectors (https://doi.org/10.5281/zenodo.6861341) -- STEREO collaboration.
	
	- the FIFRADINA datasets:  Al(nth,g) in Al2O3, Si(nth,g) in Si, Ge(nth,g) in Ge, W(nth,g) in CaWO4 for nuclear recoil calibration of cryogenic detectors (https://doi.org/10.5281/zenodo.7936552) -- CRAB collaboration.

If this library contributed to published results, please cite:

> L. Thulliez, H. Kluck et A. Bonhomme, _Fifrelin4Geant4_, (2023) [DOI:10.5281/zenodo.7933381](https://doi.org/10.5281/zenodo.7933381)

as acknowledgment.


## Documentation

Beside this README, one can have also a look at the doxygen comments in the relevant header files. It is also of advantage to be familiar with [Geant4's Book For Application Developers](https://geant4-userdoc.web.cern.ch/UsersGuides/ForApplicationDeveloper/BackupVersions/V10.6c/html/Introduction/introduction.html).

## Contact and support

If further information or support is needed, one may contact the maintainer of Fifrelin4Geant4, currently [Lo&iuml;c Thulliez (loic.thulliez@cea.fr)](mailto:loic.thulliez@cea.fr).

## Build and Install Fifrelin4Geant4

To build the Fifrelin4Geant4 library, clone the repository

	mkdir <source>
	git clone https://gitlab.com/lthullie/fifrelin4geant4.git <source>
	
where `<source>` is a temporary folder to contain the source code. Then build the library with cmake

	mkdir <build>
	cd <build>
	cmake <source> -DCMAKE_INSTALL_PREFIX=<install>
	cmake --build . --target install -j <number of cores>
	
here `<build>` is the temporary build folder and `<install>` is the install location of the built library; for a parallel build `<number of cores>` specify the number of used cores.
 
## Linking fifrelin4geant4 to a Geant4 application
 
Add the following lines to the `CMakeLists.txt` of your Geant4 application at the proper place:

	set(FIFRELIN_LIBRARY ${PROJECT_SOURCE_DIR}/fifrelin4geant4/) 
	include_directories(${FIFRELIN_LIBRARY}/include)
	target_link_libraries(your_executable ${FIFRELIN_LIBRARY}/lib/libFifrelin.so)
	
and modify your physics list (commonly named `PhysicsList.cc`) as follows:
 
	//Add to the list of header files:
	#include "FifNeutronHPCapture.hh"
	
	//Add to ConstructProcess() method:
	//Specify the nuclide(s) for which the thermal neutron capture process should be using FIFRELIN
	//data. The nuclide (Z,A) is specified by an int of the following form Z*1000+A. For example to use
	//FIFRELIN data for the 182W(n,g)183W process, it is 74*1000+182 = 74182.
	vector<int> fifrelinNuclideList_ = {74182};
	
	G4HadronicProcess* capture = 0;
	G4ProcessManager* pmanager = G4Neutron::Neutron()->GetProcessManager();
	G4ProcessVector* pv = pmanager->GetProcessList();
	for(size_t i=0; i<static_cast<size_t>(pv->size()); ++i){ 
		if( fCapture==((*pv)[i])->GetProcessSubType() ) {
			capture = static_cast<G4HadronicProcess*>((*pv)[i]);
		} 
	}
	if(!capture){ 
		capture = new G4HadronCaptureProcess("nCapture");
		pmanager->AddDiscreteProcess(capture);
	}else{
		std::vector<G4HadronicInteraction*> had=capture->GetHadronicInteractionList();
		for (size_t i=0; i<had.size() ;i++){
			printf("name =%s Emin=%f, Emax=%f\n",had[i]->GetModelName().data(),had[i]->GetMinEnergy(),had[i]->GetMaxEnergy());
			if(had[i]->GetModelName()=="NeutronHPCapture") {
				G4cout << ("ConstructProcess: change min/max energy of ") << had[i]->GetModelName() << " to 0,0" << G4endl;
				had[i]->SetMinEnergy(0.);
				had[i]->SetMaxEnergy(0.);
			}
		}
	}
	
	Fifrelin::NeutronHPCapture *theFifNeutronHPCapture = new Fifrelin::NeutronHPCapture(fifrelinNuclideList_);
	theFifNeutronHPCapture->SetMaxEnergy(20.*MeV);
	capture->RegisterMe(theFifNeutronHPCapture);
	
## Running a Geant4 application with Fifrelin4Geant4

Set the environment variable `GEANT4_FIFRELIN_DATA` to the location `<data>` of the Fifrelin data files:

	export GEANT4_FIFRELIN_DATA=<data>
	
To automatically set this, put the command in the configuration files of your shell, e.g. `.bashrc` for bash.

## Description of Fifrelin4Geant4 files

- **FifNeutronHPCapture.cc** defines a neutron capture process. It follow closely the related Geant4 standard class, except that the decay of the compound nucleus is handles by the `FifNeutronHPChannel` class. Its constructor expect an argument `fifrelinNuclideList_` of type `std::vector<int>` that specifies for which nuclides FIFRELIN data should be used.

- **FifNeutronHPChannel.cc** defines a decay channel which its final state either handled by FIFRELIN or Geant4 depending on `fifrelinNuclideList_`.

- **FifNeutronHPCaptureFS.cc** create the final state of the capture process based on the distribution of secondary particles sampled `FifNeutronHPSecDist` and creates the related secondary particle tracks on the stack.

- **FifNeutronHPSecDist.cc** samples the secondary particle distribution using FIFRELIN data loaded by `FifDataLoader` and set all properties of the emitted particle accordingly. It expect that the environment variable `GEANT4_FIFRELIN_DATA` is set and points to the location of the FIFRELIN data files.

- **FifDataLoader.cc** contains the code for reading the actual FIFRELIN data file. If the FIFRELIN file has an unsupported data format, change the code here. See also [below](#fifrelin-data-format).

- **Files.cc** provides auxiliary functions for file handling.
 
## FIFRELIN data format

The FIFRELIN file name has to be of the form: Z*1000+A (e.g. for 183W, it will be `74183.txt`) in the repository pointed by the environment variable ${GEANT4_FIFRELIN_DATA}.
By default the file format corresponds to a FIFRADINA dataset (https://doi.org/10.5281/zenodo.7936552). If you use another format do not hesitate to change in FifDataLoader.cc the method DataLoader::LoadCascades accordingly.

	
## Acknowledgments

Fifrelin4Geant4 was developed within the <a id="crab-ref">CRAB collaboration</a>
 
> L. Thulliez, D. Lhuillier et al. (CRAB Collaboration), JINST 16.7 (2021) P07032, [DOI:10.1088/1748-0221/16/07/P07032](https://doi.org/10.1088/1748-0221/16/07/P07032)

and is based on previous work that aimed for the better description of the de-excitation of gadolinium following thermal neutron capture:

> A. Bonhomme et al. (STEREO Collaboration), Eur. Phys. J. A55.10 (2019) 183, [DOI:10.1140/epja/i2019-12886-y](https://doi.org/10.1140/epja/i2019-12886-y)

> K. Zbiri et al. (Double Chooz Collaboration), (2005)

> (Chooz 1 Collaboration)

Fifrelin4Geant4 relies on <a id="fifrelin-ref">FIFRELIN</a>

> O. Litaize, O. Serot & L. Berge, Eur. Phys. J. A51.12 (2015) 177, [DOI:10.1140/epja/i2015-15177-9](https://doi.org/10.1140/epja/i2015-15177-9)

and [Geant4](https://geant4.cern.ch):

>J. Allison et al., Nucl. Instrum. Methods. Phys. Res. A835 (2016) 186-225, [DOI:10.1016/j.nima.2016.06.125](https://doi.org/10.1016/j.nima.2016.06.125)

>J. Allison et al., IEEE Trans. Nucl. Phys. 53.1 (2006) 270-278, [DOI:10.1109/TNS.2006.869826](https://doi.org/10.1109/TNS.2006.869826)

>S. Agostinelli et al., Nucl. Instrum. Methods. Phys. Res. A506.3 (2003) 250-303, [DOI:10.1016/S0168-9002(03)01368-8](https://doi.org/10.1016/S0168-9002(03)01368-8)

## Copyright and Licence

Copyright 2005 Karim Zbiri

Copyright 2019 Aur&eacute;lie Bonhomme

Copyright 2022,2023 Holger Kluck

Copyright 2022,2023 Lo&iuml;c Thulliez

This software is distributed under the terms of the GNU Lesser General Public
Licence version 2.1 (LGPL Version 2.1), copied verbatim in the file "LICENCE".

