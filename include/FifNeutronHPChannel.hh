#ifndef SIMULATION_FIFRELIN_INCLUDE_FIFNEUTRONHPCHANNEL_HH_
#define SIMULATION_FIFRELIN_INCLUDE_FIFNEUTRONHPCHANNEL_HH_

#include <G4HadProjectile.hh>
#include <G4ParticleHPFinalState.hh>
#include <G4StableIsotopes.hh>
#include <G4String.hh>
#include <G4Types.hh>
#include <G4ParticleHPIsoData.hh>
#include <G4WendtFissionFragmentGenerator.hh>
//#include <stddef.h>

namespace Fifrelin {

/**
 * @brief Neutron reaction channel
 *
 * It is identical to Geant4's G4NeutronHPChannel class with the exception that
 * Fifrelin:NeutronHPCaptureFS instead of G4NeutronHPCaptureFS is used for the final state generation of selected nuclides.
 * For all other nuclides G4NeutronCaptureFS is used.
 *
 * @warning This class requires patched versions of Geant4's G4ParticleHPCapture.hh (version 10.2.1) with changed
 * access modifier.
 */
class NeutronHPChannel {
public:
	NeutronHPChannel(G4ParticleDefinition* projectile)
	: wendtFissionGenerator(getenv("G4NEUTRON_HP_USE_WENDT_FISSION_MODEL") == NULL ? NULL : G4WendtFissionFragmentGenerator::GetInstance())
	{
		theProjectile = const_cast<G4ParticleDefinition*>(projectile);
		theChannelData = new G4ParticleHPVector;
		theBuffer = 0;
		theIsotopeWiseData = 0;
		theFinalStates = 0;
		active = 0;
		registerCount = -1;
		theFifrelinNuclideListSize = 0;
	}

	NeutronHPChannel()
	: wendtFissionGenerator(getenv("G4NEUTRON_HP_USE_WENDT_FISSION_MODEL") == NULL ? NULL : G4WendtFissionFragmentGenerator::GetInstance())
	{
		theProjectile = G4Neutron::Neutron();
		theChannelData = new G4ParticleHPVector;
		theBuffer = 0;
		theIsotopeWiseData = 0;
		theFinalStates = 0;
		active = 0;
		registerCount = -1;
		theFifrelinNuclideListSize = 0;
	}
	
	NeutronHPChannel(std::vector<int> _theFifrelinNuclideList)
	: wendtFissionGenerator(getenv("G4NEUTRON_HP_USE_WENDT_FISSION_MODEL") == NULL ? NULL : G4WendtFissionFragmentGenerator::GetInstance())
	{
		theProjectile = G4Neutron::Neutron();
		theChannelData = new G4ParticleHPVector;
		theBuffer = 0;
		theIsotopeWiseData = 0;
		theFinalStates = 0;
		active = 0;
		registerCount = -1;
		theFifrelinNuclideList = _theFifrelinNuclideList;
		theFifrelinNuclideListSize = int(theFifrelinNuclideList.size());
	}

	~NeutronHPChannel()
	{
		delete theChannelData;
		// Following statement disabled to avoid SEGV
		// theBuffer is also deleted as "theChannelData" in
		// ~G4ParticleHPIsoData.  FWJ 06-Jul-1999
		//if(theBuffer != 0) delete theBuffer;
		if(theIsotopeWiseData != 0) delete [] theIsotopeWiseData;
		// Deletion of FinalStates disabled to avoid endless looping
		// in the destructor heirarchy.  FWJ 06-Jul-1999
		//if(theFinalStates != 0)
		//{
		//  for(i=0; i<niso; i++)
		//  {
		//    delete theFinalStates[i];
		//  }
		//  delete [] theFinalStates;
		//}
		// FWJ experiment
		//if(active!=0) delete [] active;
		// T.K.
		if ( theFinalStates != 0 )
		{
			for ( G4int i = 0; i < niso; i++ )
			{
				delete theFinalStates[i];
			}
			delete [] theFinalStates;
		}
		if ( active != 0 ) delete [] active;
	}

	G4double GetXsec(G4double energy);

	G4double GetWeightedXsec(G4double energy, G4int isoNumber);

	G4double GetFSCrossSection(G4double energy, G4int isoNumber);

	inline G4bool IsActive(G4int isoNumber) {
		return active[isoNumber];
	}

	inline G4bool HasFSData(G4int isoNumber) {
		return theFinalStates[isoNumber]->HasFSData();
	}

	inline G4bool HasAnyData(G4int isoNumber) {
		return theFinalStates[isoNumber]->HasAnyData();
	}

	/**
	 * @brief Register the final state generator
	 *
	 * So far, the FS is hard coded and the argument is ignored.
	 * @param Unused
	 * @return
	 */
	G4bool Register(G4ParticleHPFinalState*);

	void Init(G4Element *theElement, const G4String dirName);

	void Init(G4Element *theElement, const G4String dirName,
			const G4String fsType);

	//void UpdateData(G4int A, G4int Z, G4int index, G4double abundance);
	void UpdateData(G4int A, G4int Z, G4int index, G4double abundance,
			G4ParticleDefinition *projectile) {
		G4int M = 0;
		UpdateData(A, Z, M, index, abundance, projectile);
	}
	;
	void UpdateData(G4int A, G4int Z, G4int M, G4int index, G4double abundance,
			G4ParticleDefinition *projectile);

	void Harmonise(G4ParticleHPVector *&theStore, G4ParticleHPVector *theNew);

	G4HadFinalState* ApplyYourself(const G4HadProjectile &theTrack,
			G4int isoNumber = -1);

	inline G4int GetNiso() {
		return niso;
	}

	inline G4double GetN(G4int i) {
		return theFinalStates[i]->GetN();
	}
	inline G4double GetZ(G4int i) {
		return theFinalStates[i]->GetZ();
	}
	inline G4double GetM(G4int i) {
		return theFinalStates[i]->GetM();
	}

	inline G4bool HasDataInAnyFinalState() {
		G4bool result = false;
		G4int i;
		for (i = 0; i < niso; i++) {
			if (theFinalStates[i]->HasAnyData())
				result = true;
		}
		return result;
	}

	void DumpInfo();

	G4String GetFSType() const {
		return theFSType;
	}

	G4ParticleHPFinalState** GetFinalStates() const {
		return theFinalStates;
	}

private:
	G4ParticleDefinition *theProjectile;

	G4ParticleHPVector *theChannelData; // total (element) cross-section for this channel
	G4ParticleHPVector *theBuffer;

	G4ParticleHPIsoData *theIsotopeWiseData; // these are the isotope-wise cross-sections for each final state.
	G4ParticleHPFinalState **theFinalStates; // also these are isotope-wise pionters, parallel to the above.
	G4bool *active;
	G4int niso;

	G4StableIsotopes theStableOnes;

	G4String theDir;
	G4String theFSType;
	G4Element *theElement;

	G4int registerCount;

	G4WendtFissionFragmentGenerator *const wendtFissionGenerator;
	
	std::vector<int> theFifrelinNuclideList;
	int 			theFifrelinNuclideListSize;
};

}

#endif /* SIMULATION_FIFRELIN_INCLUDE_FIFNEUTRONHPCHANNEL_HH_ */
