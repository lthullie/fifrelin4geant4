#ifndef FifNeutronHPCapture_h
#define FifNeutronHPCapture_h 1

#include "FifNeutronHPChannel.hh"
#include <G4HadFinalState.hh>
#include <G4HadProjectile.hh>
#include <G4HadronicInteraction.hh>
#include <G4Nucleus.hh>
#include <G4String.hh>
#include <G4Types.hh>
#include <iostream>
#include <utility>
#include <vector>

namespace Fifrelin {

/**
 * @brief Class for the neutron capture process
 *
 * It is identical to Geant4's G4NeutronHPCapture class with the exception that
 * Fifrelin:NeutronHPCaptureFS instead of G4NeutronHPCaptureFS is used for the final state generation.
 *
 * @warning This class requires patched versions of Geant4's G4ParticleHPCapture.hh (version 10.2.1) with changed
 * access modifier.
 */
class NeutronHPCapture: public G4HadronicInteraction {
public:

public:

	NeutronHPCapture();
	NeutronHPCapture(std::vector<int> _theFifrelinNuclideList);

	~NeutronHPCapture();

	G4HadFinalState* ApplyYourself(const G4HadProjectile &aTrack,
			G4Nucleus &aTargetNucleus);

	virtual const std::pair<G4double, G4double> GetFatalEnergyCheckLevels() const;

public:
	G4int GetVerboseLevel() const;
	void SetVerboseLevel(G4int);
	void BuildPhysicsTable(const G4ParticleDefinition&) override;
	virtual void ModelDescription(std::ostream& outFile) const;

private:
	std::vector<int>				theFifrelinNuclideList;
	std::vector<NeutronHPChannel*> *theCapture;
	G4String dirName;
	G4int numEle;

	G4HadFinalState theResult;
};

}

#endif
